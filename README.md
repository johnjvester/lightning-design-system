# `lightning-design-system` Repository

> The `lightning-design-system` was created to demonstrate the [Lightning Design System for React](https://react.lightningdesignsystem.com/) 
> for use with a simple [React](https://reactjs.org/) application.

## Publications

This repository is related to the following articles published on DZone.com:

* [Having Fun with the Lightning Design System for React](https://dzone.com/articles/having-fun-with-the-lightning-design-system-for-re)
* [Having (More) Fun Creating Components with the Lightning Design System for React](https://dzone.com/articles/having-more-fun-creating-components-with-the-light)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Getting Started Using This Repository

If you want to see this code in action, simply follow the steps below:

1. Clone this repository locally
2. Execute the `npm ci` command to pull down all the necessary dependencies
3. Execute `npm run start` command to run the application
4. Navigate to the http://localhost:3000 URL in your browser

Once started, the following screen should appear:

![Some Music Service Example](./resources/SomeMusicService.gif)

As a result of the `feature/custom_component_and_styling` branch, the spinner blueprint code was 
employed to begin the process of creating a custom React component.  With that new branch in merged into 
the `master` branch, the application load sequence was updated as shown below:

![Some Music Service Example - Custom Component](./resources/SomeMusicServiceV2b.gif)

## Getting Started On Your Own

If you do not wish to use this repository and start fresh, please follow the steps listed below:

1. `npx create-react-app lightning-design-system` to create the `lightning-design-system` React application
2. `cd lightning-design-system` to switch into the base folder for the 
3. `npm install react-app-rewired --save-dev` to install the `react-app-rewired` dependency
4. Update the following items in the `package.json` file:
   ```json
      "scripts": {
         "start": "react-app-rewired start",
         "build": "react-app-rewired build",
         "test": "react-app-rewired test --env=jsdom",
      }
   ```
5. Create a new file called `config-overrides.js` at the root of the project, containing the following information:
   ```javascript
       module.exports = function override(config, env) {
       config.module.rules = [
       // salesforce dependencies
       // this will compile salesforce lightning as src, not as package
       {
          test: /\.jsx?$/,
          include: [
            'node_modules/@salesforce/design-system-react',
          ].map(
            someDir => path.resolve(
               process.cwd(),
               someDir
           )
          ),
          loader: require.resolve('babel-loader'),
          options: {
             presets: [
                "react-app"
            ],
          },
       },
       ].concat(config.module.rules);
    
       return config;
       }
   ```
6. Execute `npm install --save @salesforce-ux/design-system @salesforce/design-system-react` to install Lightning Design System into this project
7. Execute the commands below to copy files into the `public` folder of this project:
   ```shell
   cp node_modules/@salesforce-ux/design-system/assets/styles/salesforce-lightning-design-system.min.css public/
   cp -r node_modules/@salesforce-ux/design-system/assets/fonts public/

   mkdir -p public/assets
   cp -r node_modules/@salesforce-ux/design-system/assets/icons public/assets

   mkdir -p public/assets
   cp -r node_modules/\@salesforce/design-system-react/assets/images public/assets/
   ```
8. Add `<link rel="stylesheet" type="text/css" href="/salesforce-lightning-design-system.min.css">` into the `index.html` for the project
9. Update the `index.jsp` as shown below:
   ```javascript
   import IconSettings from '@salesforce/design-system-react/components/icon-settings';
   
   ReactDOM.render(
      <React.StrictMode>
         <IconSettings iconPath="/assets/icons">
            <App />
         </IconSettings>
      </React.StrictMode>,
      document.getElementById('root')
   );
   ```
10. Update the `App.js` to start using Lightning Design System components
11. Execute `npm ci` command to pull down all the necessary dependencies
12. Execute `npm start` to run the application
13. Navigate to the http://localhost:3000 URL in your browser

For more information, please review the following URL:

https://github.com/salesforce/design-system-react/blob/master/docs/create-react-app-2x.md

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
