import {Spinner} from "@salesforce/design-system-react";

function SpinnerWithDescription(props) {
    return (
        <div className="slds-m-top_medium">
            <div className="slds-align_absolute-center slds-p-top_medium">
                <Spinner size="small"
                         variant="base"
                         assistiveText={{ label: props.description }}
                         isInline={true}
                         hasContainer={false} />
            </div>
            <div>
                <p className="slds-text-font_monospace">{props.description}</p>
            </div>
        </div>
    );
}

export default SpinnerWithDescription;