import './App.css';
import Badge from '@salesforce/design-system-react/components/badge';
import {
    Alert,
    AlertContainer,
    Button, DataTable, DataTableColumn, DataTableRowActions, Dropdown,
    Icon, TrialBar
} from "@salesforce/design-system-react";

import {Component} from "react";
import SpinnerWithDescription from "./SpinnerWithDescription";

class App extends Component {
    state = {
        showAlert: true,
        showSpinner: true,
        loadingMessage: "Getting Data, Please Wait ...",
        items: [
            {
                id: '0',
                albumName: 'Grace Under Pressure',
                releaseDate: '7/1/1984',
                sales: '1,000,000 (Platinum)',
                label: 'Mercury',
                credits: 13.06
            },
            {
                id: '1',
                albumName: 'Hold Your Fire',
                releaseDate: '8/8/1987',
                sales: '500,000 (Gold)',
                label: 'Mercury',
                credits: 6.99
            },
            {
                id: '2',
                albumName: 'Power Windows',
                releaseDate: '10/14/1985',
                sales: '1,000,000 (Platinum)',
                label: 'Mercury',
                credits: 7.99
            },
            {
                id: '3',
                albumName: 'Roll the Bones',
                releaseDate: '8/23/1991',
                sales: '1,000,000 (Platinum)',
                label: 'Atlantic',
                credits: 5.14
            },
            {
                id: '4',
                albumName: 'Signals',
                releaseDate: '11/10/1982',
                sales: '1,000,000 (Platinum)',
                label: 'Mercury',
                credits: 8.93
            }
        ],
    };

    componentDidMount() {
        this.timer = setTimeout(
            () => {
                this.setState({showSpinner: false});
            },
            3000,
        );
    }

    closeAlert() {
        this.setState({showAlert: false});
    }

    handleRowAction = (item, action) => {
        console.log(item, action);
    };

    render() {
        return (
            <div className="App">
                <div className="slds-text-heading_large slds-p-top_xx-large">
                    Some Music Service
                </div>
                {this.state.showSpinner &&
                <SpinnerWithDescription description={this.state.loadingMessage}/>
                }
                {this.state.showAlert &&
                <div>
                    <AlertContainer>
                        <Alert
                            icon={<Icon category="utility" name="user"/>}
                            labels={{
                                heading: 'This is an Informational Alert',
                                headingLink: 'Close this Alert',
                            }}
                            onClickHeadingLink={() => this.closeAlert()}
                        />
                    </AlertContainer>
                </div>
                }

                {!this.state.showSpinner &&
                <div className="slds-m-top_small">
                    <Badge
                        id="badge-base-example-success"
                        color="success"
                        content="2,112.00 Purchase Credits Available"
                        icon={
                            <Icon
                                category="utility"
                                name="moneybag"
                                size="xx-small"
                                colorVariant="base"
                            />
                        }
                    />
                </div>
                }

                {!this.state.showSpinner &&
                <div className="slds-m-top_small">
                    <DataTable
                        items={this.state.items}
                        id="DataTableExample-music"
                        striped
                    >
                        <DataTableColumn key="album" label="Album Name" property="albumName"/>
                        <DataTableColumn key="release-date" label="Release Date" property="releaseDate"/>
                        <DataTableColumn key="sales" label="Original Copies Sold" property="sales"/>
                        <DataTableColumn key="label" label="Label" property="label"/>
                        <DataTableColumn key="credits" label="Credits" property="credits" right="right"/>
                        <DataTableRowActions
                            options={[
                                {
                                    id: 0,
                                    label: 'Buy this Song',
                                    value: '1',
                                },
                                {
                                    id: 1,
                                    label: 'Save for Later',
                                    value: '2',
                                },
                                {
                                    id: 2,
                                    label: 'Preview this Song',
                                    value: '3',
                                }
                            ]}
                            onAction={this.handleRowAction}
                            dropdown={<Dropdown length="3"/>}
                        />
                    </DataTable>
                </div>
                }

                {!this.state.showSpinner &&
                <div className="slds-m-top_small">
                    <TrialBar
                        labels={{timeLeft: '15', timeLeftUnit: 'days'}}
                        onRenderActions={() => (
                            <Button variant="success" label="Subscribe Now"/>
                        )}
                    >
                        <div style={{marginTop: 15}}>Never miss another deal again, press the <strong>Subscribe
                            Now</strong> button to get started today.
                        </div>
                    </TrialBar>
                </div>
                }
            </div>
        )
    }
}

export default App;
